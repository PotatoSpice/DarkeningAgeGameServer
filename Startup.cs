using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using WebApplication1.Services;
using WebApplication1.Middlewares;
using Microsoft.AspNetCore.Http;
using WebApplication1.Repositories;

namespace WebApplication1
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddHttpClient();

            // # Repositories
            services.AddTransient<IGameRoomManager, GameRoomManager>();
            services.AddTransient<IRoomManager, RoomManager>();
            // services.AddTransient<IConnectionManager, ConnectionManager>();

            // # Services
            services.AddSingleton<IMessageHandlerService, MessageHandlerService>();
            services.AddTransient<IMatchMakingHandlerService, MatchMakingHandlerService>();
            // services.AddSingleton<ILobbyCommunicationService, LobbyCommunicationService>();
            // services.AddSingleton<IGameCommunicationService, GameCommunicationService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP Event pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            //app.UseHttpsRedirection();
            app.UseRouting();
            
            app.UseWebSockets();
            // app.Map("/ws-lobby", (app) => app.UseMiddleware<LobbyCommunicationMiddleware>());
            // app.Map("/ws-game", (app) => app.UseMiddleware<GameCommunicationMiddleware>());

            app.UseEndpoints(endpoints =>
            {
                endpoints.Map("/ws-lobby/{host_join}", 
                    endpoints.CreateApplicationBuilder().UseMiddleware<LobbyMiddleware>().Build());
                endpoints.Map("/ws-game", 
                    endpoints.CreateApplicationBuilder().UseMiddleware<GameMiddleware>().Build());
                endpoints.MapGet("/", async context =>
                {
                    await context.Response.WriteAsync("Hello World!");
                });
            });
        }
    }
}
