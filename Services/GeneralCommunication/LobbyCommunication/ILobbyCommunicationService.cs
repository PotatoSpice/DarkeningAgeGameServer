using System;
using System.Net.WebSockets;
using System.Threading.Tasks;

namespace WebApplication1.Services
{
    [Obsolete("LobbyCommunicationService is deprecated, please use MessageHandlerService instead.")]
    public interface ILobbyCommunicationService : ICommunicationHandlerService
    {
        new Task ReceiveAsync(WebSocket socket, string receivedData);
    }   
}