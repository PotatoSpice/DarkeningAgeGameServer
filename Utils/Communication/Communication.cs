﻿using System;

namespace WebApplication1.Utils
{
    [Obsolete("This class will use the iteration methods of the map")]
    public class Communication
    {
       /* MapManager mapManager;

        public Communication(GameInstance game)
        {
             this.mapManager = game.GetMapManager(); 
        }

        public String getRegionsByType(String type)
        {
            return JsonConverter<Region>.convertListToJsonString(mapManager.getRegionsByType(type)); 
        }
        
        public String getTypeInformations(String type)
        {
            return JsonConverter<RegionType>.convertObjectToJsonString(mapManager.searchRegionType(type)); 
        }

        public String getFactionRegions(String faction)
        {
            return JsonConverter<Region>.convertListToJsonString(mapManager.getFactionRegions(faction)); 
        }

        public String checkIfRegionsHaveBorder(String region1, String region2)
        {
           bool doThey =  mapManager.regionsHaveBorder(mapManager.searchRegionByName(region1), mapManager.searchRegionByName(region2));
           return JsonConvert.SerializeObject(doThey); //I'm praying this will work ngl
        }

        public String getBorderRegions(String region)
        {
            return JsonConverter<Region>.convertListToJsonString(mapManager.borderRegions(mapManager.searchRegionByName(region)));
        }

        public String changeRegionOwner(String newOwner, String region)
        {
            return JsonConvert.SerializeObject( 
                mapManager.changeRegionOwner(newOwner, mapManager.searchRegionByName(region)));
        }

        public String regionTerrain(String region)
        {
            return mapManager.regionTerrain(mapManager.searchRegionByName(region)); 
        }

        public String getRegionInformation(String region)
        {
            return JsonConverter<Region>.convertObjectToJsonString(mapManager.searchRegionByName(region)); 
        }
        */
    }
}
