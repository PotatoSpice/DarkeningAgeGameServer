﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Utils.Queue
{
    public class LinkedQueue<T>
    {

        /** reference to the queue's first added element (first to get removed) */
        private LinearNode<T> front;
        /** reference to the queue's last added element (last to get removed) */
        private LinearNode<T> rear;
        /** counter of queue's elements */
        private int count = 0;

        /**
         * Creates an empty queue with zero nodes.
         */
        public LinkedQueue()
        {
            rear = null;
            front = null;
            count = 0;
        }


        public LinkedQueue(T element)
        {
            rear = new LinearNode<T>(element);
            front = new LinearNode<T>(element);
            count = 1;
        }

        public void enqueue(T element)
        {
            LinearNode<T> new_elem = new LinearNode<T>(element);
            if (isEmpty())
            {
                front = new_elem;
            }
            else
            {
                rear.setNext(new_elem);
            }
            rear = new_elem;
            count++;
        }

        public T dequeue()
        {
            if (isEmpty())
            {
                return default(T);
            }
            LinearNode<T> temp = front;
            front = front.getNext();
            count--;
            return temp.getElement();
        }


        public T first()
        {
            if (isEmpty())
            {
                return default(T);
            }
            return front.getElement();
        }

        public bool isEmpty()
        {
            return (count == 0);
        }


        public int size()
        {
            return count;
        }

        public String toString()
        {
            String s = "";
            if (!isEmpty())
            {
                LinearNode<T> temp = front;
                while (temp.getNext() != null)
                {
                    s = s + "[ThisNode: " + temp.GetHashCode()
                            + ";NextNode: " + temp.getNext().GetHashCode()
                            + "; ElementContent: " + temp.getElement() + "]\n";
                    temp = temp.getNext();
                }
                s = s + "[ThisNode: " + temp.GetHashCode()
                        + "; NextNode: null"
                        + "; ElementContent: " + temp.getElement() + "]";
            }
            return s;
        }

    }
}
