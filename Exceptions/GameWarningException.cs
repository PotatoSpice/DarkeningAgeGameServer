using System;

namespace WebApplication1.Exceptions
{
    /// <summary>
    /// Represents an exception that occurs when the user inputs something wrong, while a game is running.
    /// </summary>
    public class GameWarningException : ApplicationException
    {
        private string _message = string.Empty;
    
        public string ExceptionMessage { get { return _message; } set { _message = value; } }
    
        public GameWarningException() : base() { }
    
        public GameWarningException(string message) : base(message)
        {
            _message = message;
        }
    }   
}